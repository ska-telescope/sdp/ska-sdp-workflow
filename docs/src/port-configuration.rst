Receive Process and Port Configuration
======================================

Multiple Port Configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The scripting library has the capability to configure multiple ports.
This allows to deploy a single receiver with multiple ports. In another word, this will allow a single receiver
to receive data for a single SPEAD stream coming from multiple processes.

Now, assuming each sender sends data for 1 channel and all baselines, then we'll want to have as many ports as
channels on the receiver side.  For cbf-receive, a single receiver process can receive on multiple ports already,
and this is configurable via ``reception.receiver_port_start`` and ``reception.num_ports``.