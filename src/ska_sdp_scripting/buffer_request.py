"""Buffer request class module for SDP scripting."""

import logging
from dataclasses import dataclass
from typing import List

LOG = logging.getLogger("ska_sdp_scripting")


@dataclass
class BufferRequest:
    """
    Request a buffer reservation.

    This is currently just a placeholder.

    :param size: size of the buffer
    :type size: float
    :param tags: tags describing the type of buffer required
    :type tags: list of str
    """

    size: float
    tags: List[str]

    def __post_init__(self):
        LOG.info("Buffer request, size: %s, tags: %s", self.size, self.tags)
