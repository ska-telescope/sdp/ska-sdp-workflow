"""
Common functions used to configure the QueueConnector
"""

import logging
from time import time

from ska_sdp_config import Config
from ska_sdp_config.entity.flow import FlowSource

from ska_sdp_scripting.utils import FlowStatus

LOG = logging.getLogger("ska_sdp_scripting")
TIMEOUT_SEC = 60.0


def _is_qc_tagged(source: FlowSource):
    return source.function and "ska-sdp-lmc-queue-connector" in source.function


def wait_for_queue_connector_state(
    config: Config,
    pb_id: str | None,
    expected_state: str | None,
    timeout: float,
) -> None:
    """
    Wait for all queue connector flows to reach an expected state.

    :param config: SDP configuration client
    :type  config: Config
    :param pb_id: Processing Block ID of flows
    :type  pb_id: str | None
    :param expected_state: Expected state the QueueConnector needs to reach
    :type  expected_state: str
    :param timeout: Time in seconds to elapse before TimeoutError
    :type  timeout: float
    :raises TimeoutError: Timeout exceeded wait
    """
    timeout_time = time() + timeout
    for watcher in config.watcher(timeout=timeout):
        states = []
        for txn in watcher.txn():
            for flow_key in txn.flow.list_keys(pb_id=pb_id):
                if any(
                    _is_qc_tagged(source)
                    for source in txn.flow.get(flow_key).sources
                ):
                    state = txn.flow.state(flow_key).get()
                    if state and state["status"] == FlowStatus.FAULT.value:
                        LOG.warning(
                            "Got LMC Queue Connector fault for data "
                            "flow %s with message: %s",
                            flow_key,
                            state.get("exception"),
                        )
                    states.append(
                        state.get("status") if state is not None else None
                    )

        # break if all pb flows for queue connector are in the expected state
        # or if there are no queue connector flows setup
        if all(map(lambda state: state == expected_state, states)):
            if len(states) == 0:
                LOG.info(
                    "No QueueConnector flows detected for processing block %s",
                    pb_id,
                )
            break

        # raise if timed out
        if time() > timeout_time:
            raise TimeoutError(
                f"Timeout waiting for {pb_id} queue connector flows "
                f"to reach {expected_state} state"
            )
