# pylint: disable=protected-access

"""
Functions to test the scripting flow models
"""

import pytest
from ska_sdp_config.entity.common import PVCPath, TangoAttributeUrl
from ska_sdp_config.entity.flow import (
    DataProduct,
    DataQueue,
    Display,
    Flow,
    FlowSource,
    TangoAttribute,
)

from tests.utils import create_pb_states

DATA_QUEUE = DataQueue(
    topics="pointing-offset",
    host="ska-sdp-kafka:9092",
    format="npy",
)

DATA_PRODUCT = DataProduct(
    data_dir=PVCPath(
        k8s_namespaces=["dp-shared", "dp-shared-p"],
        k8s_pvc_name="shared-storage",
        pvc_mount_path="/data",
        pvc_subpath="product/test/sdp/vis",
    ),
    paths=["out-{scan_id}-id.ms"],
)


@pytest.mark.parametrize(
    "data_type, data_model, sink",
    [
        (
            "data-product",
            "Visibility",
            DATA_PRODUCT,
        ),
        (
            "data-queue",
            "PointingTable",
            DATA_QUEUE,
        ),
        (
            "display",
            "Metrics",
            Display(
                widget="textual",
                endpoint="https://sdhp.stfc.skao.int/dpshared/qa/api/metrics",
            ),
        ),
        (
            "tango",
            "PointingTable",
            TangoAttribute(
                attribute_url="tango://mid-sdp/subarray/01/pointing_offset",
                dtype="DevFloat",
                default_value=0.0,
            ),
        ),
    ],
)
def test_data_flow(pb, data_type, data_model, sink):
    """
    Data flow entries are created correctly via
    ProcessingBlock.create_<x> correctly
    """
    assert len(pb._flows) == 0
    data_flow = pb.create_data_flow(data_type, data_model, sink)
    assert len(pb._flows) == 1

    result = pb._flows[0].flow
    assert result.key.pb_id == pb._pb_id
    assert result.key.kind == data_type
    assert result.key.name == data_type
    assert result.sink == sink
    assert len(result.sources) == 0

    new_source = FlowSource(
        uri=Flow.Key(pb_id=pb._pb_id, name="test"),
        function="tmp:tmp",
    )
    data_flow.add_source(new_source)
    assert len(result.sources) == 1
    assert result.sources[0] == new_source


def test_flow_with_source(pb):
    """
    Data flow is created with source already.
    """
    data_model = "PointingTable"
    source = FlowSource(
        uri=TangoAttributeUrl("tango://my-db:111/some-device/1/attribute"),
        function="tmp:tmp",
    )

    result = pb.create_data_flow(
        "test", data_model, DATA_QUEUE, sources=[source]
    )

    assert result.flow.sources == [source]


def test_flow_append_source(pb):
    """
    Two data flows are created and the first
    is also added as the source of the second.
    """
    data_model = "PointingTable"

    flow1 = pb.create_data_flow("my-queue", data_model, DATA_QUEUE)
    flow2 = pb.create_data_flow("my-data", data_model, DATA_PRODUCT)
    flow2.add_source(flow1, function="queue_connector:KafkaConsumerSource")

    expected_source = FlowSource(
        uri=flow1.flow.key,
        function="queue_connector:KafkaConsumerSource",
    )

    assert flow2.flow.sources[0] == expected_source
    assert len(pb._flows) == 2
    assert len(pb._flows[1].flow.sources) == 1
    assert pb._flows[1].flow.sources[0] == expected_source


def test_flow_append_dict_source(pb):
    """
    Append correct FlowSource based on input dict.
    """
    data_model = "float[3]"
    source_uri = "tango://my-db:111/my-device/3/attribute"
    source_func = "queue_connector:KafkaConsumerSource"
    expected_source = FlowSource(
        uri=TangoAttributeUrl(source_uri), function=source_func
    )

    flow = pb.create_data_flow("my-queue", data_model, DATA_QUEUE)

    # add a user-defined, non-flow-type data source
    flow.add_source({"uri": source_uri}, function=source_func)

    create_pb_states(pb._config)

    # need to use phase because that commits to the DB
    phase = pb.create_phase("Work", [])
    with phase:
        for txn in pb._config.txn():
            result = txn.flow.get(flow.flow.key)

        assert isinstance(result.sources[0].uri, TangoAttributeUrl)
        assert result.sources[0] == expected_source


def test_flow_append_incorrect_source(pb):
    """
    Cannot append a source with incorrect type to a data flow.
    """
    data_model = "PointingTable"
    flow = pb.create_data_flow("my-queue", data_model, DATA_QUEUE)

    with pytest.raises(ValueError):
        flow.add_source(
            ("wrong", "type"),
            function="ska-sdp-lmc-queue-connector:KafkaConsumerSource",
        )

    with pytest.raises(ValueError):
        flow.add_source(
            {"wrong-key": "tango://my-db:111/my-device/3/attribute"},
            function="ska-sdp-lmc-queue-connector:KafkaConsumerSource",
        )


def test_create_flow_with_phase(pb):
    """
    Test that phase correctly creates and deletes flow entries.
    We are creating two flows of the same type for the same pb
    (name has to be unique).
    It also adds the Flow object of the second one as a source of the first.
    """
    data_model = "PointingTable"
    tango_source = FlowSource(
        uri="tango://my-db:1111/some-device/1/attribute",
        function="tmp:tmp",
    )

    create_pb_states(pb._config)

    res1 = pb.create_data_flow("queue1", data_model, DATA_QUEUE)
    res2 = pb.create_data_flow(
        "queue2", data_model, DATA_QUEUE, sources=[tango_source]
    )

    # can append a DataFlow as a source,
    # and also an actual FlowSource object
    res1.add_source(
        res2,
        function="my-code:my-class",
        parameters={"param1": "value1", "param2": "value2"},
    )
    res1.add_source(tango_source)

    phase = pb.create_phase("Work", [])
    with phase:
        for txn in pb._config.txn():
            # flow entry created on phase.__enter__
            result = txn.flow.list_keys()
            assert len(result) == 2
            assert result[0].name == "queue1"
            assert result[1].name == "queue2"

        for txn in pb._config.txn():
            flow_1 = txn.flow.get(result[0])
            flow_2 = txn.flow.get(result[1])

            # we've added two sources to res1;
            # first source is res2, second source is
            # tango_source
            assert len(flow_1.sources) == 2
            assert isinstance(flow_1.sources[0].uri, Flow.Key)
            assert isinstance(flow_2.sources[0].uri, TangoAttributeUrl)
            assert len(flow_2.sources) == 1
            assert flow_1.sources[0].parameters["param1"] == "value1"

    # flow deleted on phase.__exit__
    for txn in pb._config.txn():
        result = txn.flow.list_keys()
        assert len(result) == 0
