# pylint: disable=protected-access

"""
Tests for the helm_deploy.py code
"""

from ska_sdp_scripting.utils import DeploymentStatus
from tests.utils import PB_ID


def test_deployment_is_pending(work_phase):
    """
    When a deployment is being created, its
    status in the processing block state should
    always start with PENDING
    """
    deploy_name = "cbf-sdp-emulator"
    deploy_id = f"proc-{PB_ID}-{deploy_name}"

    with work_phase:
        for txn in work_phase._config.txn():
            work_phase.ee_deploy_helm(deploy_name)

            pb_state = txn.processing_block.state(PB_ID).get()
            assert (
                pb_state["deployments"][deploy_id]
                == DeploymentStatus.PENDING.value
            )
